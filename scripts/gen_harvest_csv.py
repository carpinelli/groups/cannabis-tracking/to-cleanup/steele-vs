#! /usr/bin/env python3

"""Generates a harvest upload file in CSV format for valid Metrc."""

import string
import logging
import traceback
from pathlib import Path
from datetime import date
from datetime import timedelta
from tkinter import Tk
from tkinter.filedialog import askopenfilename

import pandas as pd
import openpyxl

"""validate Issues
    Contains "VEG" or "MOMS" lost!
"""
""".bak Issues:
    If "NOT FOUND"
    If "NO TAG"
    If "Veg"
    To capital case
    Sort only sorts old re
    itemgetters needed for new room style

   TODO
    Save 10 backups or 5
    Add proper validation (RFID, weight, etc.)
    Organize constants (maybe into a new file)
    Organize imports (maybe by dumping them?)
    Make sure all functions using type hinting
    Add debug level logging
    Make constant finder (variable not reassigned/map alteration funcs).
    Replace some lists with dictionaries
"""

# # # Globals / Logger # # #

# Logger Setup
try:
    Cwd = Path(".").resolve()
    LogDir = Cwd / "data/logs"
    LogFile = LogDir / "gen_harvest_csv.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile)  # Get file handler
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.INFO)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)

except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup

# # # Functions # # #

# # IO # #


def get_filename(Prompt: str, initialPath: Path = Path.home(),
                 Filetypes: tuple = (("Excel files", "*.xlsx"),
                                     ("All files", "*.*"))) -> str:
    """Opens a Tk filedialog window for a user to select a file, that
    file's name is returned as a string."""
    # # Constants # #
    Filetypes = (("Excel files", "*.xlsx", "*.xls"), ("All files", "*.*"))

    # # Variables # #
    root = Tk()  # get a main window so we can destroy it later
    root.withdraw()  # hide main window so only the filedialog is visible

    # Workaround to hide hidden files
    # Undocumented usage, hence the outer try/catch block and bare
    # exception
    try:
        # Call a dummy dialog using a non-existant option
        # This will not give a file dialog, but will throw a TclError
        # This will create the parent namespace necessary to call
        # set commands below
        try:
            root.tk.call("tk_getOpenFile", "-foobarbaz")
        except Exception:  # _tkinter.TclError
            pass
        # Set inner/magic variables
        root.tk.call("set", "::tk::dialog::file::showHiddenBtn", '1')
        root.tk.call("set", "::tk::dialog::file::showHiddenVar", '0')
    except Exception:
        logger.warn(traceback.format_exc())
        logger.warn("An error occurred, hidden files will now be shown...")

    # Get filename
    filename = askopenfilename(initialdir=initialPath, title=Prompt,
                               filetypes=Filetypes)

    root.destroy()  # destroy main window created earlier

    return filename


def to_excel(Dataframe: DataFrame, ExcelPath: str,
             Sheetname: str, StartRow: int) -> None:
    """Saves a pandas DataFrame to an existing Excel file."""
    with pd.ExcelWriter(ExcelPath, engine="openpyxl",
                        date_format="MM/DD/YYYY", mode='a') as writer:
        wb = openpyxl.load_workbook(ExcelPath)
        writer.book = wb
        writer.sheets = dict((ws.title, ws) for ws in wb.worksheets)
        Dataframe.to_excel(writer, sheet_name=Sheetname,
                           startrow=StartRow, header=True, index=False)

        return


# # Date / Time # #


def get_dateStr(Date: date, Separator: str = '/',
                Zero_Padding: bool = True) -> str:
    """Returns a date formatted as a string."""
    DateFmt = "%m{Separator}%d{Separator}%Y"
    Unpadded_DateFmt = "{month}{Separator}{day}{Separator}{year}"

    # Format date string with or without zero-padding
    if Zero_Padding:
        dateStr = date.strftime(Date,
                                DateFmt.format(Separator=Separator))
    else:
        dateStr = Unpadded_DateFmt.format(month=Date.month,
                                          day=Date.day,
                                          year=Date.year,
                                          Separator=Separator)

    return dateStr


def get_date(Separator: str = '/', day: str = "Today",
             Zero_Padding: bool = True, Ahead: bool = False) -> date:
    """Gets a date string using the Separator for formatting. Day can be any
    day of the week or today. If Day is set to a day of the week, then
    Ahead decides what direction to find the date in."""
    Days = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
            "Saturday", "Sunday", "Today")

    day = day.lower().capitalize()  # Ensure key is formatted correctly
    today = date.today()

    # If not today
    if day != Days[-1]:
        Index = Days.index(day)

        # Check if date is ahead of or behind today
        if Ahead:
            offset = (Index - today.weekday()) % 7  # days after today
            offsetDate = today + timedelta(days=offset)
        else:
            offset = (today.weekday() - Index) % 7  # days before today
            offsetDate = today - timedelta(days=offset)

        # Format date string with or without zero-padding
        offsetDate = get_dateStr(offsetDate, Separator=Separator,
                                 Zero_Padding=Zero_Padding)

        return offsetDate

    today = get_dateStr(today, Separator=Separator,
                        Zero_Padding=Zero_Padding)

    return today


# # Functors # #


def capitalize_strain(Strain: str) -> str:
    """Capitalize each word in strain string except 'x' for cross
    strains."""
    return " ".join([w.capitalize() if w.lower() != 'x' else w.lower() for
                    w in Strain.split(' ')])


# # Tasks # #


def validate() -> None:
    # # Constants # #

    # Columns
    Bay_Room_Parts = ["Roomletter", "Bay"]
    Tier_Room_Parts = ["Phase", "Roomletter", "Tier"]

    # Regular Expressions
    Bay_RoomRe = r"([A-S])-(\d{1,2})"
    Tier_RoomRe = r"(\d).([A-S]).(\d)"

    # Get harvest table
    HarvestPath = get_filename("Get a harvest file...")
    harvestTable = pd.read_excel(HarvestPath, header=1)

    # # Validate Data # #

    # Change RFID column to uppercase
    # # Consider changing RFID to Tag like Metrc
    harvestTable["RFID"] = harvestTable["RFID"].apply(str.upper)
    # Capitalize words in strain
    harvestTable["Strain"] = (harvestTable["Strain"]
                              .apply(capitalize_strain))
    # harvestTable[harvestTable["Strain"].str.contains(" X ")]
    # for each word in returned values if value == " X " return word.lower()
    # Change the letter in the room to uppercase
    harvestTable["Room"] = (harvestTable["Room"]
                            .apply(str.upper))

    # # Sort Data # #

    rooms = harvestTable["Room"]  # get rooms for sorting

    # Build dataframes that contains room parts for sorting
    bay_sortFrame = rooms.str.extract(Bay_RoomRe)
    tier_sortFrame = rooms.str.extract(Tier_RoomRe)

    # Check if dataframes are full of NaN
    Has_Tier_Rooms = not tier_sortFrame.dropna().empty
    Has_Bay_Rooms = not bay_sortFrame.dropna().empty

    # Setup sort frames to provide desired sort

    if Has_Bay_Rooms:
        bay_sortFrame.dropna(inplace=True)  # remove NaN for casting
        bay_sortFrame.columns = Bay_Room_Parts  # name room parts
        bay_sortFrame["Bay"].apply(int)  # cast bay to int for sorting
        bay_sortCols = Bay_Room_Parts + ["RFID"]  # sort order
        bay_ascendingCols = [True, False, True]  # ascending for each sort
        # Add sort frames to harvest table
        bay_harvestTable = pd.concat([harvestTable, bay_sortFrame],
                                     axis=1, ignore_index=False)

        # Sort values based on data built above
        bay_harvestTable.sort_values(by=bay_sortCols,
                                     ascending=bay_ascendingCols,
                                     inplace=True)
        # Remove non-tier-named data
        bay_harvestTable.dropna(inplace=True)

    if Has_Tier_Rooms:
        tier_sortFrame.dropna(inplace=True)  # remove NaN for casting
        # Name room parts
        tier_sortFrame.columns = Tier_Room_Parts
        tier_sortFrame["Phase"].apply(int)  # cast phase to int for sorting
        tier_sortFrame["Tier"].apply(int)  # cast tier to int for sorting
        tier_sortCols = Tier_Room_Parts + ["RFID"]  # sort order
        # Ascending for each sort
        tier_ascendingCols = [True for i in range(4)]
        # Add sort frames to harvest table
        tier_harvestTable = pd.concat([harvestTable, tier_sortFrame],
                                      axis=1, ignore_index=False)

        # Sort values based on data built above
        tier_harvestTable.sort_values(by=tier_sortCols,
                                      ascending=tier_ascendingCols,
                                      inplace=True)
        # Remove non-tier-named data
        tier_harvestTable.dropna(inplace=True)

    # # Comments off from here

    # Merge data tables where NaN
    if Has_Bay_Rooms and Has_Tier_Rooms:
        harvestTable = tier_harvestTable.append(bay_harvestTable,
                                                ignore_index=True,
                                                sort=False)
        harvestTable.drop(columns=(Bay_Room_Parts + Tier_Room_Parts),
                          inplace=True)
    elif Has_Bay_Rooms and not Has_Tier_Rooms:
        harvestTable = bay_harvestTable.drop(columns=Bay_Room_Parts)
    elif Has_Tier_Rooms and not Has_Bay_Rooms:
        harvestTable = tier_harvestTable.drop(columns=Tier_Room_Parts)
    else:  # not (Has_Bay_Rooms and Has_Tier_Rooms)
        raise Exception("No rooms match known patterns...")

    harvestTable.reset_index(drop=True, inplace=True)

    # # Write Data # #

    to_excel(harvestTable, HarvestPath, "Weights", StartRow=1)

    return


def gen_csv(HarvestPath: str) -> None:
    """FROM HERE
    Read and rewrite until: TO HERE"""

    # Too many blank lines...

    def getWordFromNumber(number: int) -> str:
        """Gets a spelled string from an individual digit."""
        if not str(number).isnumeric():
            raise TypeError

        number = int(number)  # ensure integer
        isOutOfRange = (number > 9 or number < 0)
        if isOutOfRange:
            raise IndexError

        return Spelled_Digits[number]

    # Too many blank lines...

    def getWordFromAbbreviation(abbreviation: str) -> str:
        """Gets a fully-spelled version of an abbreviation.
        If abbreviation is not in templates.Spelled_Words a LookupError is
        raised.
        """
        abbreviation = abbreviation.lower()

        return Spelled_Words[abbreviation]

    # Too many blank lines...

    def getSpelledStrain(Strain: str) -> str:
        """Gets a fully-spelled version of a strain with number and
        abbreviations.
        """
        spelled_strain = []

        for word in Strain.split(' '):
            word = word.strip(string.punctuation)
            if word.isnumeric():
                if int(word) in Spelled_Digits:
                    spelled_strain.append(getWordFromNumber(word))

            elif word.upper() in Spelled_Words:
                spelled_strain.append(getWordFromAbbreviation(word))

            else:
                spelled_strain.append(word)

        return " ".join(spelled_strain)

    # Too many blank lines...

    def getStrainCode(Strain: str) -> str:
        """Gets the first two characters of each word from a fully spelled
        strain name."""
        strain_code = str()

        spelled_strain = getSpelledStrain(Strain)

        for word in spelled_strain.split(' '):
            strain_code += word[:2]

        return strain_code.upper()

    # Too many blank lines...

    def getStrainCodes(Strains: list) -> list:
        """Gets a list of strain codes from a list of strains."""
        strain_codes = []

        for strain in Strains:
            strain_codes.append(getStrainCode(strain))

        return strain_codes

    # Too many blank lines...

    def convertToStripped(in_sheetlist: list) -> None:
        """Converts in_sheetlist by applying .strip() to all values"""
        for row in in_sheetlist:
            for cell_value in row:
                if isinstance(cell_value, str):
                    cell_value = cell_value.strip()

    # Too many blank lines...

    def convertNoneToString(in_sheetlist: list) -> None:
        """Converts in_sheetlist None values to empty string."""
        for row in in_sheetlist:
            for cell_value in row:
                if cell_value is None:
                    cell_value = str()

    # Too many blank lines...

    """TO HERE"""
    today = get_date(Separator='.')
    last_wednesday = get_date(day="Wednesday")
    error_moveDate = get_dateStr(date.today() - timedelta(days=Days_In_Flower))

    return


def globaLs(VarDump: None) -> None:
    # Debugging and logging
    import time
    import traceback
    import logging
    # Files and IO
    import sys
    from pathlib import Path
    from operator import itemgetter
    import csv
    # Dates and Text
    from datetime import date, timedelta
    import string  # ascii_uppercase, punctuation
    import re
    # Graphics and File Selection
    from tkinter import Tk
    from tkinter.filedialog import askopenfilename

    import xlrd
    import pandas as pd
    import openpyxl
    from openpyxl.utils import get_column_letter

    # Grow
    Days_In_Flower = 60

    # GLOBALS
    Home = Path.home()
    # Desktop = Home / "Desktop"
    Desktop = Home / "Desktop/Steele/Spreadsheets/Harvest"
    Cwd = Path(".").resolve()
    LogDir = Cwd / "data/logs"
    LogFile = LogDir / "gen_harvest_csv.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    Empty = (None, "", [], (), {})
    TagIndex = 0
    WeightIndex = 1
    RoomIndex = 3
    First_Row = 3
    Last_Row = 1000
    First_Column = TagIndex + 1
    Last_Column = RoomIndex + 1
    WeightsSheetname = "Weights"

    # Formatters
    BatchnameFmt = "{date} {State_Code} {strain_code}"
    Drying_RoomFmt = "DRYING ROOM #{Cycle}"

    """TEMPLATES"""
    # Sets #
    Blanks = (None, "")
    Cycle_Range = tuple([cycle for cycle in range(1, 15)])

    Spelled_Digits = {0: "zero", 1: "one", 2: "two", 3: "three", 4: "four",
                      5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine"}
    Spelled_Words = {"dr.": "Doctor", "dr": "Doctor"}

    # GetStrain Set
    # End Sets #

    # Strings
    Units = "Grams"
    Not_Found = "NOT FOUND"
    No_Tag = "NO TAG"
    Veg = "Veg"

    # Formatting
    Date_Format = "X%m{Separator}X%d{Separator}X%Y"
    Leading_Zero_Date_Format = "%m{Separator}%d{Separator}%Y"
    Batchname_Format = "{date} {State_Code} {strain_code}"
    Cycle_Format = "DRYING ROOM #{Cycle}"

    # Regular Expressions
    Room_Pattern = re.compile(r"([A-S])-(\d{1,2})")  # COMMENT ME
    Strain_Pattern = re.compile(r"(^\S.*?)(?=:|$)")  # COMMENT ME

    # Column Indices
    RfidIndex = 0

    StrainIndex = 2
    RoomIndex = 3

    # Data Ranges
    Start_Row = 3
    End_Row = 1000
    Start_Column = RfidIndex + 1
    End_Column = RoomIndex + 1
    # Indices
    Old_RoomletterIndex = 0
    BaynumberIndex = 1
    PhaseIndex = 0
    RoomletterIndex = 1
    TierIndex = 2

    """ __INIT__ """

    CO_State_Code = "020"

    """ EXCEL """

    # Move these constants to tgs
    # Sheetnames
    l_Weights = "Weights"
    l_Report = "Report"

    # Column Ranges
    RFID = "A3:A1000"
    WEIGHT = "B3:B1000"
    STRAIN = "C3:C1000"
    ROOM = "D3:D1000"

    # Sheet Ranges
    Weights_Range = "$A$3:$D$1000"

    # File Extensions
    XlsxExt = ".xlsx"
    CsvExt = ".csv"

    """END Templates"""


def main():
    validate()


if __name__ == "__main__":
    try:
        main()
    except Exception:
        logger.critical(traceback.format_exc())
        exit(-1)
