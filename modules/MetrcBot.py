#!/usr/bin/env python3

"""A bot to download information from METRC."""

from datetime import date
from pathlib import Path
import time

import pyautogui
from selenium import webdriver
from selenium.common import exceptions as Exceptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from IPython.core.debugger import set_trace


# User info
username = ""
password = ""
email = ""


"""Saved XPaths
report_element.find_element_by_xpath(
"//option[contains(text(), '- All Growth Phases -')]")
"""


class MetrcBot:
    def __init__(self, username, password, email):
        # # # Members # # #

        # Account Information
        self.username = username
        self.password = password
        self.email = email

        # # Constants # #

        # Path
        self.Download_Path = Path(".").resolve()
        self.Downloads = str(self.Download_Path)

        # Waiting
        self.Timeout = 10
        self.Default_Wait = self.Timeout

        # URLs
        self.Login = "/log-in?ReturnUrl=%2f"
        self.URL = "https://co.metrc.com"  # Feature: Add state template
        # Feature: Add a licenseNumber template
        self.User = f"{self.URL}/user/profile?licenseNumber=403R-00020"
        self.Homepage = f"{self.URL}/industry/403R-00020/plants"
        self.Reports = f"{self.URL}/industry/403R-00020/reports"

        # FIX XPATH
        self.User_XPath = f"//*[contains(text(), '{self.User}')]"

        self.Mime_Types = ["text/plain",
                           "text/csv",
                           "text/comma-separated-values",
                           "application/csv",
                           "application/vnd.ms-excel",
                           "application/x-msexcel",
                           "application/x-excel",
                           "application/excel",
                           "application/download",
                           "application/octet-stream",
                           "binary/octet-stream",
                           "application/binary",
                           "application/x-unknown"]
        self.Used_Types = [self.Mime_Types[1], self.Mime_Types[3],
                           self.Mime_Types[4]]


        # # Setup Browser # #

        # Firefox Preferences
        profile = webdriver.FirefoxProfile()
        mime_types = ",".join(self.Used_Types)

        preferences = {"DownloadFolderList": "browser.download.folderList",
                       "DownloadDir": "browser.download.dir",
                       "DownloadAlertOnExeOpen": "browser.download.alertOnEXEOpen",
                       "ManagerAlertOnExeOpen": "browser.download.manager.alertOnEXEOpen",
                       "NeverAskSaveToDisk": "browser.helperApps.neverAsk.saveToDisk",
                       "ManagerShowWhenStarting": "browser.download.manager.showWhenStarting",
                       "ManagerFocusWhenStarting": "browser.download.manager.focusWhenStarting",
                       "AlwaysAskForce": "browser.helperApps.alwaysAsk.force",
                       "ManagerCloseWhenDone": "browser.download.manager.closeWhenDone",
                       "ManagerShowAlertOnComplete": "browser.download.manager.showAlertOnComplete",
                       "ManagerUseWindow": "browser.download.manager.useWindow",
                       "ManagerShowWhenStarting": "browser.download.manager.showWhenStarting",
                       "ServicesManagerShowWhenStart": ("services.sync.prefs.sync.browser.download"
                                                        ".manager.showWhenStarting"),
                       "DisablePDFJavaScript": "pdfjs.disabled"}

        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.dir", self.Downloads)
        # profile.set_preference("browser.download.alertOnEXEOpen", False)
        # profile.set_preference("browser.download.manager.alertOnEXEOpen",
        #                        False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                               mime_types)
        profile.set_preference("browser.download.manager.showWhenStarting",
                               False)
        profile.set_preference("browser.download.manager.focusWhenStarting",
                               False)
        profile.set_preference("browser.helperApps.alwaysAsk.force",
                               False)
        profile.set_preference("browser.download.manager.closeWhenDone",
                               False)
        profile.set_preference("browser.download.manager.closeWhenDone",
                               False)
        profile.set_preference("browser.download.manager.showAlertOnComplete",
                               False)
        profile.set_preference("browser.download.manager.useWindow",
                               False)
        profile.set_preference("browser.download.manager.showWhenStarting",
                               False)
        profile.set_preference("services.sync.prefs.sync.browser.download"
                               ".manager.showWhenStarting",
                               False)
        profile.set_preference("pdfjs.disabled", True)

        # Start Browser
        self.bot = webdriver.Firefox(profile)

    def isLoggedIn(self):
        try:
            self.bot.find_element_by_class_name("icon-user")

        except Exceptions.NoSuchElementException:
            return False

        else:
            return True

    def login(self):
        bot = self.bot

        bot.get(self.URL + self.Login)

        username = bot.find_element_by_id("username")
        password = bot.find_element_by_id("password")
        email = bot.find_element_by_id("email")

        username.clear()
        password.clear()
        email.clear()

        username.send_keys(self.username)
        password.send_keys(self.password)
        email.send_keys(self.email, Keys.RETURN)

        # Wait for login to load
        # WebDriverWait(bot, self.Timeout).until(EC.url_changes(self.URL
        #                                                  + self.Login))
        time.sleep(self.Default_Wait)

    def downloadReport(self, Type="Inventory", Date_Range=12):
        bot = self.bot

        # check if logged in
        if not self.isLoggedIn():
            print("AssertionError: Must be logged in!")
            raise AssertionError

        bot.get(self.Reports)

        # FIND AND CLICK REPORTS TAB

        wait = WebDriverWait(bot, 10)
        startdate = wait.until(
            EC.presence_of_element_located(
                (By.XPATH,
                 "//input[@ng-model='line.PlantsInventoryStartDate']")))
        enddate = wait.until(
            EC.presence_of_element_located(
                (By.XPATH,
                 "//input[@ng-model='line.PlantsInventoryEndDate']")))

        startdate.clear()
        enddate.clear()

        todays_date = date.strftime(date.today(), "%m/%d/%Y")
        last_year = todays_date[:-1] + str(int(todays_date[-1]) - 1)

        startdate.send_keys(last_year)
        enddate.send_keys(todays_date)

        excel_xpath = ("//button[@ng-click=\"preload.methods."
                       "viewPlantsInventoryReport(line, 'excel');\"]")
        download_report = bot.find_element_by_xpath(excel_xpath)
        download_report.click()

        # AUTOGUI CLICK OKAY

        # SAVE
        # "//*[contains(text(), '{self.User}')]")


# Hash!
ops = MetrcBot(username, password, email)

ops.login()
if not ops.isLoggedIn():
    print("ERROR! Login Failed...")
    ops.bot.quit()
    exit(1)

ops.downloadReport()
